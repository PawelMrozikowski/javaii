package database;

import logs.Log;
import properties.*;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Artur on 03.06.17.
 */
public class SqliteDatabase extends Database {
    protected static Database database;
    private static Connection connection;
    private static Statement statement;
    private Log log;
    public static void setDefault(Database _database) {
        Database.database = _database;
    }

    public SqliteDatabase()
    {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:Baza.db");
            statement = connection.createStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log = Log.getInstance();
        log.log("Polaczono z baza danych");
        //createTables();
    }

    public static SqliteDatabase getInstance()
    {
        if(database == null)
        {
            database = new SqliteDatabase();
        }
        return (SqliteDatabase)database;
    }

    public void createTables() {
        String temperatureTable = "CREATE TABLE IF NOT EXISTS Temperature " +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT, valueOf VARCHAR(10), dateOf VARCHAR(10))";
        String humidityTable = "CREATE TABLE IF NOT EXISTS Humidity(id INTEGER PRIMARY KEY AUTOINCREMENT, valueOf VARCHAR(10), dateOf VARCHAR(20))";
        String atmosphericPressureTable="CREATE TABLE IF NOT EXISTS AtmosphericPressure(id INTEGER PRIMARY KEY AUTOINCREMENT, valueOf VARCHAR(10), dateOf VARCHAR(20))";
        String windTable="CREATE TABLE IF NOT EXISTS Wind(id INTEGER PRIMARY KEY AUTOINCREMENT, valueOf VARCHAR(10), dateOf VARCHAR(20))";
        try {
            statement.execute(temperatureTable);
            statement.execute(humidityTable);
            statement.execute(atmosphericPressureTable);
            statement.execute(windTable);
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static Database getDefault() {
        return Database.database;
    }

    public Temperature getTemperature(int id, String name, String date) {
        return (Temperature) WeatherCreator.create("temperature", id, name, date);
    }
    public AtmosphericPressure getAtmosphericPressure(int id, String name, String date) {
        return (AtmosphericPressure) WeatherCreator.create("atmospheric pressure", id, name, date);
    }
    public Humidity getHumidity(int id, String name, String date) {
        return (Humidity) WeatherCreator.create("humidity", id, name, date);
    }
    public Wind getWind(int id, String name, String date) {
        return (Wind) WeatherCreator.create("wind", id, name, date);
    }

    public Temperature getTemperatureValue(String date) { //do zrobienia
        return null;
    }
    public AtmosphericPressure getAtmosphericPressureValue(String date) { //do zrobienia
        return null;
    }
    public Humidity getHumidityValue(String date) { //do zrobienia
        return null;
    }
    public Wind getWindValue(String date) { //do zrobienia
        return null;
    }

    public void insertTemperature(Temperature temperature) { //do zrobienia
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Temperature (id, valueOf, dateOf) VALUES (NULL, ?, ?);");
            preparedStatement.setString(1, temperature.getName());
            preparedStatement.setString(2, temperature.getDate());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Temperature> selectTemperature()
    {
        List<Temperature> list = new LinkedList<Temperature>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Temperature");
            while(resultSet.next()){
                int id = resultSet.getInt("id");
                String valueOf = resultSet.getString("valueOf");
                String dateOf = resultSet.getString("dateOf");
                list.add(new Temperature(id, valueOf, dateOf));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void insertAtmosphericPressure(AtmosphericPressure atmosphericPressure) { //do zrobienia
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO AtmosphericPressure (id, valueOf, dateOf) VALUES (NULL, ?, ?);");
            preparedStatement.setString(1, atmosphericPressure.getName());
            preparedStatement.setString(2, atmosphericPressure.getDate());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<AtmosphericPressure> selectAtmosfecicPressure()
    {
        List<AtmosphericPressure> list = new LinkedList<AtmosphericPressure>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM AtmosphericPressure");
            while(resultSet.next()){
                int id = resultSet.getInt("id");
                String valueOf = resultSet.getString("valueOf");
                String dateOf = resultSet.getString("dateOf");
                list.add(new AtmosphericPressure(id, valueOf, dateOf));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void insertHumidity(Humidity humidity) { //do zrobienia
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Humidity (id, valueOf, dateOf) VALUES (NULL, ?, ?);");
            preparedStatement.setString(1, humidity.getName());
            preparedStatement.setString(2, humidity.getDate());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Humidity> selectHumidity()
    {
        List<Humidity> list = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Humidity");
            while(resultSet.next()){
                int id = resultSet.getInt("id");
                String valueOf = resultSet.getString("valueOf");
                String dateOf = resultSet.getString("dateOf");
                list.add(new Humidity(id, valueOf, dateOf));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void insertWind(Wind w) { //do zrobienia
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Wind(id, valueOf, dateOf) VALUES (NULL, ?, ?);");
            preparedStatement.setString(1, w.getName());
            preparedStatement.setString(2, w.getDate());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Wind> selectWind()
    {
        List<Wind> list = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Wind");
            while(resultSet.next()){
                int id = resultSet.getInt("id");
                String valueOf = resultSet.getString("valueOf");
                String dateOf = resultSet.getString("dateOf");
                list.add(new Wind(id, valueOf, dateOf));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void closeConnection()
    {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log.log("Zamknieto polacznie z baza danych");
    }
}
