package database;

import properties.AtmosphericPressure;
import properties.Humidity;
import properties.Temperature;
import properties.Wind;

/**
 * Created by Artur on 03.06.17.
 */
public abstract class Database {
    protected static Database database;
    public static void setDefault(Database _database) {
        Database.database = _database;
    }
    public static Database getDefault() {
        return Database.database;
    }
    public abstract Temperature getTemperature(int id, String name, String date);
    public abstract AtmosphericPressure getAtmosphericPressure(int id, String name, String date);
    public abstract Humidity getHumidity(int id, String name, String date);
    public abstract Wind getWind(int id, String name, String date);
    //SELECT WHERE
    public abstract Temperature getTemperatureValue(String date);
    public abstract AtmosphericPressure getAtmosphericPressureValue(String date);
    public abstract Humidity getHumidityValue(String date);
    public abstract Wind getWindValue(String date);
    //INSERT INTO
    public abstract void insertTemperature(Temperature temperature);
    public abstract void insertAtmosphericPressure(AtmosphericPressure atmosphericPressure);
    public abstract void insertHumidity(Humidity humidity);
    public abstract void insertWind(Wind wind);
}
