package properties;

/**
 * Created by Artur on 03.06.17.
 */
public class RainFall extends WeatherData{

    private int id;
    private String valueOf;
    private String dateOf;

    public RainFall(int id, String valueOf, String dateOf) {
        this.id = id;
        this.valueOf = valueOf;
        this.dateOf = dateOf;
    }

    @Override
    public String getName() {
        return valueOf;
    }

    @Override
    public String getDate() {
        return dateOf;
    }

    @Override
    public int getId() {
        return id;
    }
}
