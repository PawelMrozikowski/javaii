package properties;

/**
 * Created by Artur on 03.06.17.
 */
public class Humidity extends WeatherData{

    private int id;
    private String valueOf;
    private String dateOf;

    public Humidity(int id, String valueOf, String dateOf) {
        this.id = id;
        this.valueOf = valueOf;
        this.dateOf = dateOf;
    }

    @Override
    public String getName() {
        return valueOf;
    }

    public void setName(String valueOf){
        this.valueOf = valueOf;
    }

    @Override
    public String getDate() {
        return dateOf;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Humidity{" +
                "id=" + id +
                ", valueOf=" + valueOf +
                ", dateOf='" + dateOf + '\'' +
                '}';
    }
}