package properties;
/**
 * Created by Artur on 03.06.17.
 */


import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import logs.Log;

import java.io.IOException;

import static com.sun.xml.internal.ws.api.message.Packet.Status.Request;

public class WebClient {
    private String JsonData;
    private Log log;

    public WebClient() {
        JsonData=null;
        log = Log.getInstance();
    }

    public void fetchJsonData() throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request=new Request.Builder().url("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22cracow%2C%20pl%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys").build();
        Response response=null;
        try {
            response = client.newCall(request).execute();
        } catch(IOException e) {
            e.printStackTrace();
        }
        JsonData=response.body().string();
        log.log("Polaczono z serwisem");
    }

    public String getJsonData() {
        return JsonData;
    }

    public String getDate() {
        JsonObject o=Json.parse(JsonData).asObject();
        log.log("Pobrano date");
        return ( ( ( o.get("query").asObject()).get("results").asObject()).get("channel").asObject()).getString("lastBuildDate", "");
    }
    public String getTemperature() {
        JsonObject o=Json.parse(JsonData).asObject();
        log.log("Pobrano temperature");
        return ( ( ( ( ( o.get("query").asObject()).get("results").asObject()).get("channel").asObject()).get("item").asObject()).get("condition").asObject()).asObject().getString("temp", "");
    }
    public String getHumidity() {
        JsonObject o= Json.parse(JsonData).asObject();
        log.log("Pobrano wilgotnosc");
        return ( ( ( ( o.get("query").asObject()).get("results").asObject()).get("channel").asObject()).get("atmosphere").asObject() ).asObject().getString("humidity", "");
    }
    public String getAtmosphericPressure() {
        JsonObject o= Json.parse(JsonData).asObject();
        log.log("Pobrano cisnienie atmosferyczne");
        return ( ( ( ( o.get("query").asObject()).get("results").asObject()).get("channel").asObject()).get("atmosphere").asObject() ).asObject().getString("pressure", "");
    }
    public String getWind() {
        JsonObject o= Json.parse(JsonData).asObject();
        log.log("Pobrano sile wiatru");
        return ( ( ( ( o.get("query").asObject()).get("results").asObject()).get("channel").asObject()).get("wind").asObject() ).asObject().getString("speed", "");
    }
}
